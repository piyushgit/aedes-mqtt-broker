'use strict';

const Aedes = require('aedes'),
    ws = require('websocket-stream'),
    logging = require('aedes-logging');

const port = 1883, wsPort = 8883;

const aedes = Aedes();
const server = require('net').createServer(aedes.handle);
const wsServer = require('http').createServer();

ws.createServer({server: wsServer}, aedes.handle);

wsServer.listen(wsPort, function () {
    console.log('Aedes websocket server listening on port', wsPort);
});

server.listen(port, function () {
    console.log('Aedes server listening on port', port);
});

aedes.on('client', function (client) {
    console.log('new client', client);
});

aedes.on('clientDisconnect', async function (client) {
    console.log('disconnect', client);
});

